#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-27 08:18:55 lukas>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import logging
import warnings
import itertools
import numpy as np
import xarray as xr
from scipy import signal
import statsmodels.api as sm
from scipy.signal import savgol_filter

import matplotlib.pyplot as plt

from utils_python.decorators import vectorize
from utils_python.xarray import area_weighted_mean, SelectRegion
from utils_python.utils import set_logger
from utils_python.get_filenames import Filenames

logger = logging.getLogger(__name__)
REGION = None# ['NEU', 'CEU', 'MED']

@vectorize('(n)->(n)')
def detrend(data):
    if np.any(np.isnan(data)):
        return data * np.nan
    return signal.detrend(data)


@vectorize('(n)->()')
def variance(data):
    return np.nanvar(data, ddof=1)


@vectorize('(n)->(n)', excluded=[1, 'polyorder', 'mode'])
def running_mean(data, window, polyorder=0, mode='interp'):
    """Fits a smoothed polyorder polynomial to the data. Defaults to a running
    mean.

    Parameters:
    - data (array): Array to fit.
    - window (int): Odd integer.
    - polyorder=0 (int, optional): Polynomial order to fit. Default (0)
      implements a running mean.
    - mode='mirror' (['mirror' | 'interp'], optional): String setting the
      the handling of the edges.
      * mirror: "Repeats the values at the edges in reverse order. The value
        closest to the edge is not included." -> use this for residuals
      * interp: "A degree polyorder polynomial is fit to the last window
        values of the edges, and this polynomial is used to evaluate the last
        window // 2 output values." -> use this for data

    Returns:
    array"""
    if window == 1:
        return data
    return signal.savgol_filter(
        data,
        window_length=window,
        polyorder=polyorder,
        cval=np.nan,
        mode=mode)


@vectorize('(n)->(n)', excluded=[1])
def lowess(data, window):
    """Fit a LOWESS filter for a given window size"""
    return sm.nonparametric.lowess(
        endog=data,
        exog=np.arange(len(data)),
        frac=float(window) / float(len(data)),
        it=3,
        delta=0.,
        is_sorted=True,
        missing='raise',
        return_sorted=False,)


def load_model_ensembles2():
    """CESM 84 members"""

    path = '/net/n2o/climphys1/fischeer/CMIP5/EXTREMES/CESM12-LE/STATS'
    filename = 'tas_CESM12-LE_historical_{}_1940-2099.YEARMEAN.GLOMEAN.nc'
    pic = '/net/bio/climphys/fischeer/CMIP5/EXTREMES/CESM12-CTL/tas_CESM12-LE_pictl.0500-5282.nc'
    dss = []

    for ensemble in range(84):
        ensemble = 'r{}i1p1'.format(ensemble)
        ds = xr.open_dataset(os.path.join(path, filename.format(ensemble)))
        ds.rename({'TREFHT': 'tas', 'time': 'year'}, inplace=True)
        ds['model_ensemble'] = xr.DataArray(
            ['CESM12-LE_{}'.format(ensemble)], dims='model_ensemble')
        ds['model'] = xr.DataArray(['CESM12-LE'], dims='model_ensemble')
        ds['ensemble'] = xr.DataArray([ensemble], dims='model_ensemble')
        ds['piControl'] = xr.DataArray([pic], dims='model_ensemble')
        ds = ds.squeeze()
        dss.append(ds)
    ds = xr.concat(dss, dim='ensemble')

    ds['tas'] -= ds['tas'].sel(year=slice('1976', '2005')).mean('year')
    ds = ds.sel(year=slice('1976', None))
    return ds


def load_models():
    # model with more than one piControl run -> select manually
    ctr_manual = {
        'CCSM4': '/net/atmos/data/cmip5-ng/tas/tas_ann_CCSM4_piControl_r1i1p1_g025.nc',
        'GISS-E2-H': '/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-H_piControl_r1i1p3_g025.nc',
        'GISS-E2-R': '/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-R_piControl_r1i1p141_g025.nc',
    }

    scenarios = ['rcp26', 'rcp45', 'rcp85']
    fn = Filenames()
    fn.apply_filter(varn='tas', scenario=scenarios + ['piControl'],
                    average='ann', grid='g025')
    models = fn.get_variable_values('model', subset={'scenario': scenarios + ['piControl']})
    if 'EC-EARTH' in models:
        models.remove('EC-EARTH')
    # models = models[:10]  # DEBUG
    fn.apply_filter(model=models)
    ds_scenarios = []
    selreg = SelectRegion(region=REGION)
    for scenario in scenarios:
        ds_models = []
        for model in models:
            for idx, ensemble in enumerate(fn.get_variable_values(
                    'ensemble', subset={'model': model, 'scenario': scenarios})):
                filename = fn.get_filenames(
                    subset={'model': model, 'scenario': scenario, 'ensemble': ensemble})
                if len(filename) != 1:
                    import ipdb; ipdb.set_trace()
                ds = xr.open_dataset(filename[0])
                ds['year'].data = ds['year'].dt.year

                ds = selreg.select_region(ds, average=True)

                ds['model_ensemble'] = xr.DataArray(
                    ['{}_{}'.format(model, ensemble)], dims='model_ensemble')
                ds['model'] = xr.DataArray([model], dims='model_ensemble')
                ds['ensemble'] = xr.DataArray([ensemble], dims='model_ensemble')

                fn_ctr = fn.get_filenames(subset={'model': model, 'scenario': 'piControl'})
                if len(fn_ctr) == 1:
                    fn_ctr = fn_ctr[0]
                else:
                    fn_ctr = ctr_manual[model]

                ds['piControl'] = xr.DataArray([fn_ctr], dims='model_ensemble')
                ds_models.append(ds)
                # TODO: comment in for only one member per model
                break
        ds = xr.concat(ds_models, dim='model_ensemble')
        ds['scenario'] = xr.DataArray([scenario], dims='scenario')
        ds_scenarios.append(ds)
    ds = xr.concat(ds_scenarios, dim='scenario')

    ds['tas'] -= ds['tas'].sel(year=slice('1976', '2005')).mean('year')
    ds = ds.sel(year=slice('1976', None))
    return ds, selreg


def get_control_period_variance(da, period_length=None, realizations=1):
    """NOTE: I think it might make sense to not take the entire control period
    to calculate the variance but only the same length as the forced
    period. But in order to still utilize all of it one can use multiple
    realizations of it an then use the mean variance from all of them"""
    if period_length is None or period_length == da.year.size:
        return xr.apply_ufunc(variance, da, input_core_dims=[['year']])

    assert period_length < .9*da.year.size, 'period_length too long'
    variances = []
    for _ in range(realizations):
        idx_start = np.random.randint(da.year.size - period_length)
        idx_end = idx_start + period_length
        da_sel = da.isel(year=range(idx_start, idx_end))
        variances.append(
            xr.apply_ufunc(variance, da_sel, input_core_dims=[['year']]))
    da = xr.concat(variances, dim='realizations')
    return da.median('realizations')


def optimize_residual_variance(ds, selreg):
    """Optimizes the filter window size in order to fit the residual
    variance to the pre-industrial control run variance"""

    def get_difference(res, ctl, smoothing_windows):
        differences = []
        if isinstance(smoothing_windows, int):
            smoothing_windows = [smoothing_windows]
        # fit variances for different smoothing windows (i.e., time scales)
        for window in smoothing_windows:
            res_smooth = xr.apply_ufunc(
                running_mean, res, window,
                kwargs={'polyorder': 0, 'mode': 'mirror'},
                input_core_dims=[['year'], []],
                output_core_dims=[['year']])
            ctl_smooth = xr.apply_ufunc(
                running_mean, ds_ctl['tas'], window,
                kwargs={'polyorder': 0, 'mode': 'mirror'},
                input_core_dims=[['year'], []],
                output_core_dims=[['year']])

            res_var = xr.apply_ufunc(
                variance, res_smooth, input_core_dims=[['year']])
            ctl_var = get_control_period_variance(ctl_smooth, res.year.size, 100)

            diff = (res_var-ctl_var) / ctl_var

            # NOTE: the results depends quite heavily on the exact method here:
            # mean, median, min, max...
            diff = np.abs(diff.median('model_ensemble').data)
            differences.append(diff)

        return np.sum(differences)  # NOTE: where exactly to put the abs(); if anywhere at all...
    # --- /get_difference/ ---


    ds['forced_response'] = xr.DataArray(np.empty_like(ds['tas'].data), dims=ds['tas'].dims)
    ds['forced_response_window'] = xr.DataArray(np.empty_like(ds['model'].data, dtype=int),
                                                dims=ds['model'].dims)
    ds['forced_response_members'] = xr.DataArray(np.empty_like(ds['model'].data, dtype=int),
                                                 dims=ds['model'].dims)
    MAX_WINDOW = 51
    for model in np.unique(ds['model'].data.ravel()):
        idx = np.where(ds.isel(scenario=0)['model'].data == model)[0]
        windows = range(1, MAX_WINDOW+1, 2)  # TODO: make this dependent on the number of ensemble members
        for i_sce, scenario in enumerate(ds['scenario'].data):
            ds_sel = ds.isel(model_ensemble=idx).sel(scenario=scenario)

            with warnings.catch_warnings():
                warnings.simplefilter('ignore')  # suppress SerializationWarning: Unable to decode time axis...
                ds_ctl = xr.open_dataset(ds_sel['piControl'].data[0])
            ds_ctl = selreg.select_region(ds_ctl['tas'], average=True).to_dataset()
            ds_ctl = xr.apply_ufunc(
                detrend, ds_ctl,
                input_core_dims=[['year']],
                output_core_dims=[['year']])

            diff_prior = 999
            forced_prior = None
            for window in windows:
                # --- TODO: running mean ---
                # forced = xr.apply_ufunc(
                #     running_mean, ds_sel.mean('model_ensemble')['tas'], window,
                #     kwargs={'polyorder': 0, 'mode': 'constant'},
                #     input_core_dims=[['year'], []],
                #     output_core_dims=[['year']])
                # --- TODO: OR lowess ---
                forced = xr.apply_ufunc(
                    lowess, ds_sel.mean('model_ensemble')['tas'], window,
                    input_core_dims=[['year'], []],
                    output_core_dims=[['year']])
                diff = get_difference(ds_sel['tas'] - forced, ds_ctl['tas'], smoothing_windows=[1, 11])
                # print(get_difference(ds_sel['tas'] - forced, ds_ctl['tas'], smoothing_windows=[1, 11]))  # DEBUG
                if diff <= diff_prior:
                    diff_prior = diff
                    forced_prior = forced
                else:  # last window was better, use it and end loop
                    ds['forced_response'].data[i_sce, idx, :] = forced_prior
                    ds['forced_response_window'].data[i_sce, idx] = window-2
                    break

            ds['forced_response_members'].data[i_sce, idx] = len(idx)
            if window == MAX_WINDOW:
                ds['forced_response'].data[i_sce, idx, :] = forced
                ds['forced_response_window'].data[i_sce, idx] = window
                logger.warning('No minimum difference found, loop ended at w={}'.format(MAX_WINDOW))

    return ds


def average_ensembles(ds, varn):
    """This is one way of utilizing multiple ensemble members & making sure
    that the model uncertainty for members of one model is zero: average over
    the ensemble members."""
    models = np.unique(ds.isel(scenario=0)['model'].data)
    ds_models = []
    for model in models:
        ds_sel = xr.Dataset(coords={'model_ensemble': model})
        idx = np.where(ds.isel(scenario=0)['model'].data == model)[0]
        # get all members of model and average them
        ds_sel[varn] = ds.isel(**{'model_ensemble': idx}).mean(
            'model_ensemble', keep_attrs=True)[varn]
        ds_models.append(ds_sel)
    return xr.concat(ds_models, dim='model_ensemble')


def internal_variability(ds):

    @vectorize('(n,m)->()')
    def _variance(data):
        return np.nanvar(data, ddof=1)

    @vectorize('(n)->()', excluded=['weights'])
    def _average(data, weights):
        return np.average(data, weights=weights)

    ds['variance_residuals'] = xr.apply_ufunc(
        _variance, ds['residuals'],
        input_core_dims=[['scenario', 'year']])
    ds_var = average_ensembles(ds, 'variance_residuals')
    if 'weights' in ds.variables:
        weights = average_ensembles(ds, 'weights')['weights'].data
    else:
        weights = None

    ds['internal_variability'] = xr.apply_ufunc(
        _average, ds_var['variance_residuals'],
        input_core_dims=[['model_ensemble']],
        kwargs=dict(weights=weights))


def scenario_uncertainty(ds):

    @vectorize('(n)->()', excluded=['weights'])
    def _average(data, weights):
        return np.average(data, weights=weights)

    @vectorize('(n)->()')
    def _variance(data):
        return np.var(data, ddof=1)

    ds_var = average_ensembles(ds, 'forced_response')
    if 'weights' in ds.variables:
        weights = average_ensembles(ds, 'weights')['weights'].data
    else:
        weights = None
    ds['model_mean_weighted'] = xr.apply_ufunc(
        _average, ds_var['forced_response'],
        input_core_dims=[['model_ensemble']],
        kwargs=dict(weights=weights))

    ds['scenario_uncertainty'] = xr.apply_ufunc(
        _variance, ds['model_mean_weighted'],
        input_core_dims=[['scenario']])


def model_uncertainty(ds):

    @vectorize('(n)->()', excluded=['weights', 'biased'])
    def _variance(data, weights=None, biased=False):
        """Implements a (biased/unbiased) weighted variance
        https://stackoverflow.com/questions/2413522/weighted-standard-deviation-in-numpy#2415343
        https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_variance
        """
        if weights is None:
            weights = np.ones_like(data)
        av = np.average(data, weights=weights)
        if biased:
            return np.average((data-av)**2, weights=weights)
        else:
            v1, v2 = np.sum(weights), np.sum(weights**2)
            return np.sum(weights * (data - av)**2) / (v1 - (v2/v1))

    @vectorize('(n)->()')
    def _average(data):
        return np.average(data)

    ds_var = average_ensembles(ds, 'forced_response')
    if 'weights' in ds.variables:
        weights = average_ensembles(ds, 'weights')['weights'].data
    else:
        weights = None
    ds['variance_models'] = xr.apply_ufunc(
        _variance, ds_var['forced_response'],
        input_core_dims=[['model_ensemble']],
        kwargs=dict(weights=weights, biased=False))

    # if there is only one model (e.g., if cc.ensemble_average==True) set it to zero
    if ds_var.dims['model_ensemble'] == 1:
        ds['variance_models'].data = np.zeros_like(ds['variance_models'].data)

    ds['model_uncertainty'] = xr.apply_ufunc(
        _average,  ds['variance_models'],
        input_core_dims=[['scenario']])


def calc_residuals(ds):
    """Calculate & smooth the residuals following
    Eq. 1 from Hawkins and Sutton 2009"""

    @vectorize('(n)->(n)', excluded=['window'])
    def _running_mean(yy):
        """Smooths data with a running mean."""
        return savgol_filter(yy, window_length=11,
                             polyorder=0, mode='mirror')

    ds['residuals'] = ds['tas'] - ds['forced_response']
    # ds['residuals'] = xr.apply_ufunc(
    #     _running_mean, ds['residuals'],
    #     input_core_dims=[['year']],
    #     output_core_dims=[['year']])

def main():
    set_logger(level='INFO')

    ds, selreg = load_models()

    ds = optimize_residual_variance(ds, selreg)

    # NOTE: example plot 4Lea
    plt.plot(ds.isel(model_ensemble=0).isel(scenario=0).tas)
    plt.plot(ds.isel(model_ensemble=0).isel(scenario=0).forced_response)
    plt.savefig('./test.png')

    calc_residuals(ds)
    internal_variability(ds)
    scenario_uncertainty(ds)
    model_uncertainty(ds)

    ds.to_netcdf('./tas_test.nc')


if __name__ == '__main__':
    main()
