#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-08-09 14:01:36 lukas>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

TODO:
- add support for (lat, lon) dimensions (separate weight for each)
- add support for sigma
"""
import os
import xarray as xr
import numpy as np
import logging
import regionmask
from configparser import ConfigParser
import argparse

from scipy.signal import detrend as scipy_detrend

from utils.get_filenames import Filenames
from utils.decorators import vectorize
import utils.utils as utils
from estimate_variance import fit_to_data

logger = logging.getLogger(__name__)
utils.set_logger(level=logging.INFO)

DATAPATH = '/net/h2o/climphys/lukbrunn/Data/ModelNames'


@vectorize('(n)->(n)')
def detrend(data):
    if np.any(np.isnan(data)):
        return data * np.nan
    return scipy_detrend(data)


def select_region(ds):
    da = ds[cc.varn]

    if cc.region is not None:
        logger.debug('Get mask...')
        mask = regionmask.defined_regions.srex.mask(da, wrap_lon=True)
        logger.debug('Get mask... DONE')
        logger.debug('Get index...')
        idxs = regionmask.defined_regions.srex.map_keys(cc.region)
        logger.debug('Get index... DONE')
        logger.debug('Combine indices...')
        mask = np.sum([mask == idx for idx in idxs], axis=0, dtype=bool)
        logger.debug('Combine indices... DONE')
        logger.debug('Select...')
        da = da.where(mask)
        da.attrs['region'] = ', '.join(cc.region)
        logger.debug('Select... DONE')

    if cc.country is not None:
        mask = regionmask.defined_regions.natural_earth.countries_50.mask(
            da, wrap_lon=True)
        idxs = regionmask.defined_regions.natural_earth.countries_50.map_keys(
            cc.country)
        da = da.where(
            np.sum([mask == idx for idx in idxs], axis=0, dtype=bool))
        da.attrs['country'] = ', '.join(cc.country)

    if cc.land_sea_mask is not None:
        mask = regionmask.defined_regions.natural_earth.land_110.mask(da) == 1
        da.attrs['land_sea'] = 'land_masked'
        if cc.land_sea_mask == 'land':
            mask = ~mask
            da.attrs['land_sea'] = 'sea_masked'
        da = da.where(mask)

    if cc.grid_point is not None:
        idx_lat = np.argmin(da['lat'].data - cc.grid_point[0])
        idx_lon = np.argmin(da['lon'].data - cc.grid_point[1])
        da = da.isel(lat=idx_lat).isel(lon=idx_lon)
        da.attrs['location'] = str(cc.grid_point[2])
        del ds['lat'], ds['lon']

    if np.all(np.isnan(da)):
        logmsg = 'All grid points masked. Wrong combination of masks?'
        logger.warning(logmsg)

    if cc.average_region and cc.grid_point is None:
        da = utils.area_weighted_mean(da.to_dataset())[cc.varn]
        del ds['lat'], ds['lon']

    ds[cc.varn] = da
    return ds


@vectorize('(n),(m)->()')
def fit(arr, target):
    if np.any(np.isnan(arr)) or np.any(np.isnan(target)):
        return np.nan
    return fit_to_data(
        arr, target, log_tests=cc.log_tests,
        smoothing_windows=cc.smoothing, cpus=10,
        full_output=True, method=cc.method)[1]['window_length_minimum'].data


def read_config():

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        dest='config', nargs='?', default='DEFAULT',
        help='')
    args = parser.parse_args()
    global cc
    cc = utils.read_config(args.config, './config_fit_lowess.ini')
    utils.log_parser(cc)

    if not isinstance(cc.season_ids, list):
        cc.season_ids = [cc.season_ids]


def cut_piControl(ds, fn):
    """cut beginning if necessary (as indicated by QF)"""
    qf = fn['qf_piControl'].data
    cut = fn['cut_piControl'].data
    if qf == 1:
        return ds.isel(time=slice(cut, None))
    return ds


def main():
    read_config()

    fn = xr.open_dataset(os.path.join(DATAPATH, cc.modelfile))
    fn = fn.isel(**{cc.model_name: np.where(fn['qf_piControl'] < 2)[0]})

    ds_model = []
    for mod in fn[cc.model_name].data:
        logger.info('Processing model: {}...'.format(mod))

        logger.info('Open & pre-process control run...')
        filename = str(fn.sel(**{cc.model_name: mod}).sel(scenario='piControl')['filename'].data)
        ds_control = xr.open_dataset(filename, decode_cf=False)
        ds_control = cut_piControl(ds_control, fn.sel(**{cc.model_name:mod}))
        ds_control = select_region(ds_control)
        ds_control[cc.varn] = xr.apply_ufunc(
            detrend, ds_control[cc.varn],
            input_core_dims=[['time']],
            output_core_dims=[['time']])
        # time dimension name must not match ds_forced time dimension name since length is different!
        ds_control = ds_control.rename({'time': 'time2'})
        logger.info('Open & cut control run... DONE')

        ds_sce = []
        for sce in fn['scenario'].data:
            logger.info('Processing scenario: {}...'.format(sce))

            logger.info('Open and cut forced run...')
            filename =  str(fn.sel(**{cc.model_name:mod}).sel(scenario=sce)['filename'].data)
            ds_forced = xr.open_dataset(filename, decode_cf=False)
            ds_forced = select_region(ds_forced)

            if cc.fit_period is not None and sce != 'piControl':
                cc.fit_period = [str(xx) for xx in cc.fit_period]
                ds_forced = ds_forced.sel(time=slice(*cc.fit_period))
            logger.info('Open and cut forced run... DONE')

            logger.info('Apply fit function to data...')
            da = xr.apply_ufunc(fit, ds_forced[cc.varn], ds_control[cc.varn],
                                input_core_dims=[['time'], ['time2']])
            ds = da.to_dataset()
            ds = ds.rename({cc.varn: 'window_length_minimum'})
            logger.info('Apply fit function to data... DONE')

            ds['scenario'] = xr.DataArray([sce], dims='scenario')
            ds_sce.append(ds)
            logger.info('Processing scenario: {}... DONE'.format(sce))
        ds = xr.concat(ds_sce, dim='scenario')
        ds[cc.model_name] = xr.DataArray([mod], dims=cc.model_name)
        ds_model.append(ds)
        logger.info('Processing model: {}... DONE'.format(mod))

    logger.info('Merging models & adding metadata...')
    ds_results = xr.concat(ds_model, dim=cc.model_name)
    ds_results['ensemble'] = fn['ensemble']
    if 'model' in fn.data_vars.keys():
        ds_results['model'] = fn['model']

    ds_results.attrs['modelfile'] = os.path.join(DATAPATH, cc.modelfile)
    if cc.region is not None:
        ds_results.attrs['region'] = ', '.join(cc.region)
    if cc.country is not None:
        ds_results.attrs['country'] = ', '.join(cc.country)
    if cc.land_sea_mask is not None:
        if cc.land_sea_mask == 'sea':
            ds_results.attrs['land_sea'] = 'land_masked'
        elif cc.land_sea_mask == 'land':
            ds_results.attrs['land_sea'] = 'sea_masked'
    if cc.grid_point is not None:
        ds_results.attrs['location'] = ', '.join(map(str, cc.grid_point))
    logger.info('Merging models & adding metadata... DONE')

    filename = '{}.nc'.format(cc.config)
    ds_results.to_netcdf(os.path.join(cc.savepath, filename))

    logger.info('Success! Saved file: {}'.format(filename))


if __name__ == '__main__':
    main()
