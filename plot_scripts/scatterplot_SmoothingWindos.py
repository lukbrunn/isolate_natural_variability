#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-24 18:02:51 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import xarray as xr
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

DATAPATH = '/net/h2o/climphys/lukbrunn/Data/IsolateNaturalVariability'
PLOTPATH = '/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/'

def plot(dss):

    models = list(dss.values())[0]['model'].data

    cmap = plt.cm.get_cmap('viridis', len(dss))
    colors = list(cmap(np.arange(len(dss))))

    fig = plt.figure(figsize=(11.69,8.27))  # A4 landscape
    ax = fig.add_subplot(111)

    for i_model, model in enumerate(models):
        for i_ds, ds in enumerate(dss.values()):
            ds = ds.sel(model=model).sel(season_id=0)

            ax.scatter(i_model*(len(dss)+1) + i_ds,
                       ds.sel(scenario='rcp26')['window_length_minimum'].data,
                       marker='o', color=colors[i_ds], s=20)
            ax.scatter(i_model*(len(dss)+1) + i_ds,
                       ds.sel(scenario='rcp45')['window_length_minimum'].data,
                       marker='+', color=colors[i_ds], s=60)
            ax.scatter(i_model*(len(dss)+1) + i_ds,
                       ds.sel(scenario='rcp85')['window_length_minimum'].data,
                       marker='x', color=colors[i_ds], s=40)

    ax.scatter([], [], marker='o', color='gray', label='RCP 2.6')
    ax.scatter([], [], marker='+', color='gray', label='RCP 4.5')
    ax.scatter([], [], marker='x', color='gray', label='RCP 8.5')
    for i_key, key in enumerate(dss.keys()):
        ax.scatter([], [], marker='s', color=colors[i_key], label=key)
    ax.legend(bbox_to_anchor=(1.04,1), loc='upper left')
    plt.subplots_adjust(right=0.85)

    ax.set_xlabel('Models')
    ax.set_xlim(-1, len(models)*len(dss)+1)
    ax.set_xticks(range(len(dss), len(models)*(len(dss)+1), 5))
    # ax.set_xticks(range(0, len(models)*(len(dss)+1), 1), minor=True)
    ax.set_xticklabels(models)
    fig.autofmt_xdate()

    ax.set_ylabel('Window size')
    ax.set_yticks(range(20, 121, 20))

    ax.grid()

    plt.title('Window size for different smoothings')
    plt.savefig(os.path.join(PLOTPATH, 'WindowSize-Smoothing.png'), dpi=300)
    plt.clf()
    plt.close()


def main():

    dss = {}

    filenames = {
        '11 years': 'tas_DJF_g025_1ens_26-45-85-piC_lowess_11.nc',
        '21 years': 'tas_DJF_g025_1ens_26-45-85-piC_lowess_21.nc',
        '31 years': 'tas_DJF_g025_1ens_26-45-85-piC_lowess_31.nc',
        'together': 'tas_DJF_g025_1ens_26-45-85-piC_lowess_11-21-31.nc'
    }

    for key, fn in filenames.items():
        dss[key] = xr.open_dataset(os.path.join(DATAPATH, fn))
    plot(dss)

if __name__ == '__main__':
    main()
