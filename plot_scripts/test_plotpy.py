#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-27 16:47:24 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:


https://plot.ly/python/getting-started/
"""
import os
import logging
import argparse
import numpy as np
import xarray as xr
from scipy import stats

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import plotly
import plotly.graph_objs as go

from utils.utils import set_logger, area_weighted_mean
from utils.get_filenames import Filenames
from isolate_natural_variability.estimate_variance import calculate_lowess, _smooth

logger = logging.getLogger(__name__)
LOGLEVEL = logging.INFO

AVERAGE_PERIOD = 'sea'
METHOD = 'lowess'

DATAPATH = '/net/h2o/climphys/lukbrunn/Data/IsolateNaturalVariability'
PLOTPATH = '/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/'

def residues_settings(ax, ylim):
    ax.set_xlim(1870, 2100)
    ax.set_ylim(-ylim, ylim)
    ax.axhline(0, color='k')


def plot(arrs, fits):

    traces = []

    time = arrs[0].sel(time=slice(None, '2005'))['time'].dt.year.data
    data = arrs[0].sel(time=slice(None, '2005'))[varn].data
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='Historical',
        line = dict(
            color = 'gray')
    ))

    time = arrs[0].sel(time=slice('2005', None))['time'].dt.year.data
    data = arrs[0].sel(time=slice('2005', None))[varn].data
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 2.6',
        line = dict(
            color = 'blue')
    ))

    data = arrs[1].sel(time=slice('2005', None))[varn].data
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 4.5',
        line = dict(
            color = 'green')
    ))

    data = arrs[2].sel(time=slice('2005', None))[varn].data
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 8.5',
        line = dict(
            color = 'red')
    ))

    time = arrs[0]['time'].dt.year.data
    data = fits[0]
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 2.6 fit',
        line = dict(
            color = 'blue')
    ))

    data = fits[1]
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 4.5 fit',
        line = dict(
            color = 'green')
    ))

    data = fits[2]
    traces.append(go.Scatter(
        x=time,
        y=data,
        mode='lines',
        name='RCP 8.5 fit',
        line = dict(
            color = 'red')
    ))


    layout = dict(title='Global mean, annual mean temperatures and LOWESS fits',
              xaxis = dict(title = 'Year'),
              yaxis = dict(title = 'Temperature (K)'))

    plotly.offline.plot(traces, filename='test', auto_open=False)




def read_input():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
        dest='filename', metavar='FILENAME', type=str,
        help='Valid netCDF filename relative to {}/varn'.format(DATAPATH))

    parser.add_argument(
        '--title', '-t', dest='title', default=None, type=str,
        help='If given set plot title manually')

    global args
    args = parser.parse_args()

    logmsg = 'Read parser input: \n\n'
    for ii, jj in sorted(vars(args).items()):
        logmsg += '  {}: {}\n'.format(ii, jj)
    logger.info(logmsg)


def main():
    set_logger(level=LOGLEVEL)
    read_input()

    global varn
    varn = args.filename.split('_')[0]

    ds_results = xr.open_dataset(os.path.join(DATAPATH, args.filename))

    ds_info = xr.open_dataset(ds_results.attrs['modelfile'])

    models = list(set(ds_info['model-ensemble'].data).intersection(
        ds_results['model-ensemble'].data))

    model = 'CanESM2-r1i1p1'
    season_id = 0

    ds_r = ds_results.sel(**{'model-ensemble': model}).sel(season_id=season_id)

    ds_26 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': model}).sel(
        scenario='rcp26')['filename'].data)).sel(season_id=season_id)

    ds_45 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': model}).sel(
        scenario='rcp45')['filename'].data)).sel(season_id=season_id)

    ds_85 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': model}).sel(
        scenario='rcp85')['filename'].data)).sel(season_id=season_id)

    ds_26 = area_weighted_mean(ds_26)
    ds_45 = area_weighted_mean(ds_45)
    ds_85 = area_weighted_mean(ds_85)

    arr_26 = ds_26[varn].data
    fit_26 = calculate_lowess(
        60,
        #ds_r.sel(scenario='rcp26')['window_length_minimum'].data,
        arr_26)

    arr_45 = ds_45[varn].data
    fit_45 = calculate_lowess(
        60,
        #ds_r.sel(scenario='rcp45')['window_length_minimum'].data,
        arr_45)

    arr_85 = ds_85[varn].data
    fit_85 = calculate_lowess(
        60,
        #ds_r.sel(scenario='rcp85')['window_length_minimum'].data,
        arr_85)

    plot([ds_26, ds_45, ds_85], [fit_26, fit_45, fit_85])


if __name__ == '__main__':
    main()
