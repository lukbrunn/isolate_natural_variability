#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-25 08:41:11 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import pickle
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

def load(fielname):
    with open(filename, 'rb') as f:
        return pickle.load(f)


def plot_variance(dd, key_x, key_y):

    cmap = plt.cm.get_cmap('viridis', len(dd.keys()))
    colors = list(cmap(np.arange(len(dd.keys()))))

    xx = np.array([dd[key]['rcp26'][0][key_x]
                   for key in dd.keys()])
    arr1 = [dd[key]['rcp26'][0][key_y] for key in dd.keys()]
    arr2 = [dd[key]['rcp45'][0][key_y] for key in dd.keys()]
    arr3 = [dd[key]['rcp85'][0][key_y] for key in dd.keys()]

    plt.scatter(xx, arr1,  marker='+', color=colors, s=60)
    plt.scatter(xx, arr2,  marker='o', color=colors, s=20)
    plt.scatter(xx, arr3,  marker='x', color=colors, s=40)

    plt.gca().set_xlabel('Control period variance (K$^2$)')

    plt.gca().set_ylabel('LOWESS filter window size (years)')
    plt.gca().set_ylim(50, 150)
    plt.gca().set_yticks(np.arange(50, 151, 20))

    slope1, intercept1, rvalue1, pvalue1, err1 = stats.linregress(xx, arr1)
    slope2, intercept2, rvalue2, pvalue2, err2 = stats.linregress(xx, arr2)
    slope3, intercept3, rvalue3, pvalue3, err3 = stats.linregress(xx, arr3)
    plt.plot(xx,
             np.mean([intercept1 + slope1*xx,
                      intercept2 + slope2*xx,
                      intercept3 + slope3*xx], axis=0),
             color='k', ls='-', label='Least squares')

    # slope1, intercept1, _, _ = stats.theilslopes(arr1, xx, alpha=.9)
    # slope2, intercept2, _, _ = stats.theilslopes(arr2, xx, alpha=.9)
    # slope3, intercept3, _, _ = stats.theilslopes(arr3, xx, alpha=.9)

    # plt.plot(xx, intercept1 + slope1*xx, color='k', ls='--', label='RCP 2.6')
    # plt.plot(xx, intercept2 + slope2*xx, color='k', ls=':', label='RCP 4.5')
    # plt.plot(xx, intercept3 + slope3*xx, color='k', ls='-', label='RCP 8.5')
    # plt.plot(xx,
    #          np.mean([intercept1 + slope1*xx,
    #                   intercept2 + slope2*xx,
    #                   intercept3 + slope3*xx], axis=0),
    #          color='k', ls='-', label='Theil-Sen')

    plt.scatter([], [], marker='+', color='gray', label='RCP 2.6')
    plt.scatter([], [], marker='o', color='gray', label='RCP 4.5')
    plt.scatter([], [], marker='x', color='gray', label='RCP 8.5')

    if key_x == 'target_variance_high':
        plt.gca().set_xticks(np.arange(.02, .11, .01))
        plt.gca().set_title('High frequency variance (slope p-value={:.3f})'.format(
]            np.mean([pvalue1, pvalue2, pvalue3])))
    elif key_x == 'target_variance_low':
        plt.gca().set_xlim(.005, .019)
        plt.gca().set_xticks(np.arange(.001, .020, .002))
        plt.gca().set_title('Low frequency variance (slope= p-value={:.3f})'.format(
            np.mean([pvalue1, pvalue2, pvalue3])))

    plt.legend()

    plt.savefig('/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/{}-{}.png'.format(key_x, key_y), dpi=300)
    plt.clf()
    plt.close()

def plot_rcp(dd):

    cmap = plt.cm.get_cmap('viridis', len(dd.keys()))
    colors = list(cmap(np.arange(len(dd.keys()))))

    arr1 = [dd[key]['rcp26'][0]['window_size_minimum'] for key in dd.keys()]
    arr2 = [dd[key]['rcp45'][0]['window_size_minimum'] for key in dd.keys()]
    arr3 = [dd[key]['rcp85'][0]['window_size_minimum'] for key in dd.keys()]

    arr_max = np.max([arr1, arr2, arr3], axis=0)

    arr1 /= arr_max
    arr2 /= arr_max
    arr3 /= arr_max

    xx = np.ones_like(arr1)

    bplot = plt.boxplot(
        [arr1, arr2, arr3], positions=range(3),
        # plot vertical
        vert = True,
        # whiskers at 5th & 95 percentila
        whis = [5, 95],
        # box widths
        # widths = [.2, .4, .6],
        # x labels
        labels = ['RCP 2.6', 'RCP 4.5', 'RCP 8.5'],
        # patch_artist=True,
        # median
        medianprops = dict(
            linestyle = '-',
            linewidth = .5,
            color = 'black'),
        # median
        showmeans = True,
        meanline = True,
        meanprops = dict(
            linestyle = '-',
            #linewidth = .5,
            #color = 'red'
        ),
        # # whiskers
        # whiskerprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # capprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # # fliers
        # flierprops = dict(
        #     marker = 'x')
    )

    plt.gca().set_ylabel('Normalized LOWESS filter window size')

    plt.gca().set_title('Window size (normalized to the maximum per model) for different RCPs')

    plt.legend(
        [bplot['medians'][0], bplot['means'][0]],
        ['median', 'mean'])
    plt.savefig('/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/rcp-window_size.png', dpi=300)
    plt.clf()
    plt.close()

def plot_spread(dd):

    cmap = plt.cm.get_cmap('viridis', len(dd.keys()))
    colors = list(cmap(np.arange(len(dd.keys()))))

    arr1 = [dd[key]['rcp26'][0]['window_size_minimum'] for key in dd.keys()]
    arr2 = [dd[key]['rcp45'][0]['window_size_minimum'] for key in dd.keys()]
    arr3 = [dd[key]['rcp85'][0]['window_size_minimum'] for key in dd.keys()]

    rcp26_mean = np.mean(arr1)
    rcp45_mean = np.mean(arr2)
    rcp85_mean = np.mean(arr3)
    model_mean = np.mean([arr1, arr2, arr3], axis=0)

    bplot = plt.boxplot(
        [arr1-rcp26_mean, arr2-rcp45_mean, arr3-rcp85_mean, [],
         np.array(arr1)-model_mean, arr2-model_mean, arr3-model_mean], positions=range(7),
        # plot vertical
        vert = True,
        # whiskers at 5th & 95 percentila
        whis = [5, 95],
        # box widths
        # widths = [.2, .4, .6],
        # x labels

        # patch_artist=True,
        # median
        medianprops = dict(
            linestyle = '-',
            linewidth = .5,
            color = 'black'),
        # median
        showmeans = True,
        meanline = True,
        meanprops = dict(
            linestyle = '-',
            #linewidth = .5,
            #color = 'red'
        ),
        # # whiskers
        # whiskerprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # capprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # # fliers
        # flierprops = dict(
        #     marker = 'x')
    )

    plt.gca().set_xticks([0, 1, 2, 4, 5, 6])
    plt.gca().set_xticklabels(['RCP 2.6', 'RCP 4.5', 'RCP 8.5', 'RCP 2.6', 'RCP 4.5', 'RCP 8.5'])

    plt.gca().set_ylabel('LOWESS filter window size anomaly')
    plt.gca().set_xlabel('Deviation from the model mean   |   Deviation from the scenario mean')

    plt.gca().set_ylim(-60, 60)
    ticks = np.arange(-60, 61, 20)
    ticks[0] = -55
    plt.gca().set_yticks(ticks)
    labels = np.arange(-60, 61, 20)
    labels = list(map(str, labels))
    labels[0] = 'Mean'
    plt.gca().set_yticklabels(labels)
    plt.gca().text(0, -55, rcp26_mean, horizontalalignment='center', verticalalignment='center')
    plt.gca().text(1, -55, rcp45_mean, horizontalalignment='center', verticalalignment='center')
    plt.gca().text(2, -55, rcp85_mean, horizontalalignment='center', verticalalignment='center')

    plt.gca().set_title('Spread in the filter window size across models and RCPs')

    plt.legend(
        [bplot['medians'][0], bplot['means'][0]],
        ['median', 'mean'])
    plt.savefig('/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/window_size-spread.png', dpi=300)
    plt.clf()
    plt.close()


def plot_seasons(dd):
    # TODO

    for sea in [0, 3, 6, 9]:
        arr1 = [dd[key]['rcp26'][sea][0]['window_size_minimum'] for key in dd.keys()]
        arr2 = [dd[key]['rcp45'][sea][0]['window_size_minimum'] for key in dd.keys()]
        arr3 = [dd[key]['rcp85'][sea][0]['window_size_minimum'] for key in dd.keys()]

    rcp26_mean = np.mean(arr1)
    rcp45_mean = np.mean(arr2)
    rcp85_mean = np.mean(arr3)
    model_mean = np.mean([arr1, arr2, arr3], axis=0)

    bplot = plt.boxplot(
        [arr1-rcp26_mean, arr2-rcp45_mean, arr3-rcp85_mean, [],
         np.array(arr1)-model_mean, arr2-model_mean, arr3-model_mean], positions=range(7),
        # plot vertical
        vert = True,
        # whiskers at 5th & 95 percentila
        whis = [5, 95],
        # box widths
        # widths = [.2, .4, .6],
        # x labels

        # patch_artist=True,
        # median
        medianprops = dict(
            linestyle = '-',
            linewidth = .5,
            color = 'black'),
        # median
        showmeans = True,
        meanline = True,
        meanprops = dict(
            linestyle = '-',
            #linewidth = .5,
            #color = 'red'
        ),
        # # whiskers
        # whiskerprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # capprops = dict(
        #     color = 'blue',
        #     linestyle = ':'),
        # # fliers
        # flierprops = dict(
        #     marker = 'x')
    )

    plt.gca().set_xticks([0, 1, 2, 4, 5, 6])
    plt.gca().set_xticklabels(['RCP 2.6', 'RCP 4.5', 'RCP 8.5', 'RCP 2.6', 'RCP 4.5', 'RCP 8.5'])

    plt.gca().set_ylabel('LOWESS filter window size anomaly')
    plt.gca().set_xlabel('Deviation from the model mean   |   Deviation from the scenario mean')

    plt.gca().set_ylim(-60, 60)
    ticks = np.arange(-60, 61, 20)
    ticks[0] = -55
    plt.gca().set_yticks(ticks)
    labels = np.arange(-60, 61, 20)
    labels = list(map(str, labels))
    labels[0] = 'Mean'
    plt.gca().set_yticklabels(labels)
    plt.gca().text(0, -55, rcp26_mean, horizontalalignment='center', verticalalignment='center')
    plt.gca().text(1, -55, rcp45_mean, horizontalalignment='center', verticalalignment='center')
    plt.gca().text(2, -55, rcp85_mean, horizontalalignment='center', verticalalignment='center')

    plt.gca().set_title('Spread in the filter window size across models and RCPs')

    plt.legend(
        [bplot['medians'][0], bplot['means'][0]],
        ['median', 'mean'])
    plt.savefig('/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/window_size-spread.png', dpi=300)
    plt.clf()
    plt.close()

def main():

    filename = './save_parameters.pkl'

    dd = load(filename)

    # plot_variance(dd, 'target_variance_high', 'window_size_minimum')
    # plot_variance(dd, 'target_variance_low', 'window_size_minimum')

    # plot_rcp(dd)

    plot_spread(dd)


if __name__ == '__main__':
    main()
