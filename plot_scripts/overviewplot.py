#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-30 16:26:21 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import logging
import argparse
import numpy as np
import xarray as xr
from scipy import stats
import regionmask

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import utils.utils as utils
from utils.get_filenames import Filenames
from isolate_natural_variability.estimate_variance import lowess, running_mean

xr.set_options(enable_cftimeindex=True)

logger = logging.getLogger(__name__)
LOGLEVEL = logging.INFO

AVERAGE_PERIOD = 'sea'
METHOD = 'lowess'

DATAPATH = '/net/h2o/climphys/lukbrunn/Data/IsolateNaturalVariability'
PLOTPATH = '/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/'

def residues_settings(ax, ylim):
    ax.set_xlim(1870, 2100)
    ax.set_ylim(*ylim)
    ax.axhline(0, color='k')


def get_ylim(ax):
    ylim = np.max(np.abs(ax.get_ylim()))
    ylim = np.around(ylim, 1)
    return (-ylim, ylim)


def plot(ds_r, target, arrs, fits, filename, title):

    fig, axes = plt.subplots(3,2, figsize=(10, 12))
    years = np.arange(1870, 2101)

    axes[0, 0].set_title('Temperature residues (K) control period')
    for window in ds_r['smoothing_window'].data:
        axes[0, 0].plot(np.arange(len(target)), running_mean(window, target), lw=1.)
    axes[0, 0].set_xlim(len(target)-len(years), len(target))
    ylim = get_ylim(axes[0, 0])
    axes[0, 0].set_ylim(*ylim)
    axes[0, 0].axhline(0, color='k')

    axes[0, 1].set_title('Absolute temperatures and fits (K)')
    colors = ['darkblue', 'darkgreen', 'darkred']
    for idx, (arr, fit) in enumerate(zip(arrs, fits)):
        axes[0, 1].plot(years, fit.data, lw=1., color=colors[idx])
        axes[0, 1].plot(np.arange(2005, 2101), arr[135:],
                        lw=.5, color=colors[idx])
    axes[0, 1].plot(np.arange(years[0], 2006), arrs[0][:136],
                    lw=.5, color='gray')
    axes[0, 1].set_xlim(1870, 2100)

    axes[1, 0].set_title('Temperature residues (K) RCP 2.6')
    for window in ds_r['smoothing_window'].data:
        axes[1, 0].plot(years, running_mean(window, arrs[0] - fits[0]).data, lw=1.)
    residues_settings(axes[1, 0], ylim)

    axes[1, 1].set_title('Temperature residues (K) RCP 4.5')
    for window in ds_r['smoothing_window'].data:
        axes[1, 1].plot(years, running_mean(window, arrs[1] - fits[1]), lw=1.)
    residues_settings(axes[1, 1], ylim)

    axes[2, 0].set_title('Temperature residues (K) RCP 8.5')
    for window in ds_r['smoothing_window'].data:
        axes[2, 0].plot(years, running_mean(window, arrs[2] - fits[2]), lw=1.)
    residues_settings(axes[2, 0], ylim)

    for ax in axes.ravel():
        ax.set_xlabel('')
        ax.set_ylabel('')
        for tick in ax.get_xticklabels():
            tick.set_rotation(0)

    text = 'Variable\Scenario\n\n'
    for varn in ds_r.data_vars:
        if varn in ['difference', 'ensemble', 'model', 'quality']:
            continue
        text += '{}:\n'.format(varn)
        if 'smoothing_window' in ds_r[varn].dims:
            for window in ds_r['smoothing_window'].data:
                text += '  {}:\n'.format(window)
    text += '\nMean value residuals:\n\n'
    text += 'Control period length:\n'
    text += 'Control period Gaussian:'
    axes[2, 1].text(-.1, 1., text, verticalalignment='top')

    text = 'RCP 2.6\n\n'
    for varn in ds_r.data_vars:
        if varn == 'window_length_minimum':
            text += '{}\n'.format(ds_r.sel(scenario='rcp26')[varn].data)
        elif 'smoothing_window' in ds_r[varn].dims:
            text += '\n'
            for window in ds_r['smoothing_window'].data:
                tt = ds_r.sel(scenario='rcp26').sel(smoothing_window=window)[varn].data
                if varn in ['bartlett', 'permutation'] and tt < .05:
                    text += '{:.4f}*\n'.format(tt)
                else:
                    text += '{:.4f}\n'.format(tt)
    text += '\n{:.4f}\n\n'.format(np.mean(arrs[0] - fits[0]))
    text += '{}\n'.format(len(target))
    if stats.normaltest(target).pvalue < .05:
        text += 'False'
    else:
        text += 'True'

    axes[2, 1].text(.5, 1., text, verticalalignment='top')

    text = 'RCP 4.5\n\n'
    for varn in ds_r.data_vars:
        if varn == 'window_length_minimum':
            text += '{}\n'.format(ds_r.sel(scenario='rcp45')[varn].data)
        elif 'smoothing_window' in ds_r[varn].dims:
            text += '\n'
            for window in ds_r['smoothing_window'].data:
                tt = ds_r.sel(scenario='rcp45').sel(smoothing_window=window)[varn].data
                if varn in ['bartlett', 'permutation'] and tt < .05:
                    text += '{:.4f}*\n'.format(tt)
                else:
                    text += '{:.4f}\n'.format(tt)
    text += '\n{:.4f}\n\n'.format(np.mean(arrs[1] - fits[1]))
    axes[2, 1].text(.7, 1., text, verticalalignment='top')

    text = 'RCP 8.5\n\n'
    for varn in ds_r.data_vars:
        if varn == 'window_length_minimum':
            text += '{}\n'.format(ds_r.sel(scenario='rcp85')[varn].data)
        elif 'smoothing_window' in ds_r[varn].dims:
            text += '\n'
            for window in ds_r['smoothing_window'].data:
                tt = ds_r.sel(scenario='rcp85').sel(smoothing_window=window)[varn].data
                if varn in ['bartlett', 'permutation'] and tt < .05:
                    text += '{:.4f}*\n'.format(tt)
                else:
                    text += '{:.4f}\n'.format(tt)
    text += '\n{:.4f}\n\n'.format(np.mean(arrs[2] - fits[2]))
    axes[2, 1].text(.9, 1., text, verticalalignment='top')

    axes[2, 1].axis('off')
    axes[2, 1].set_xlim(0, 1)
    axes[2, 1].set_ylim(0, 1)

    fig.suptitle(title, fontsize='x-large')

    plt.savefig(os.path.join(PLOTPATH, filename), dpi=300)
    logger.info(os.path.join(PLOTPATH, filename))
    plt.clf()
    plt.close()


def read_input():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
        dest='filename', metavar='FILENAME', type=str,
        help='Valid netCDF filename relative to {}/varn'.format(DATAPATH))

    parser.add_argument(
        '--title', '-t', dest='title', default=None, type=str,
        help='If given set plot title manually')

    global args
    args = parser.parse_args()

    logmsg = 'Read parser input: \n\n'
    for ii, jj in sorted(vars(args).items()):
        logmsg += '  {}: {}\n'.format(ii, jj)
    logger.info(logmsg)


def select_region(ds, ds_r):
    da = ds[varn]

    if 'region' in ds_r.attrs:
        region = ds_r.attrs['region'].split(', ')
        mask = regionmask.defined_regions.srex.mask(da, wrap_lon=True)
        idxs = regionmask.defined_regions.srex.map_keys(region)
        mask = np.sum([mask == idx for idx in idxs], axis=0, dtype=bool)
        # NOTE: for this to work all variables have to depend on the same dims
        da = da.where(mask)

    if 'country' in ds_r.attrs:
        country = ds_r.attrs['country'].split(', ')
        mask = regionmask.defined_regions.natural_earth.countries_50.mask(
            da, wrap_lon=True)
        idxs = regionmask.defined_regions.natural_earth.countries_50.map_keys(
            country)
        da = da.where(
            np.sum([mask == idx for idx in idxs], axis=0, dtype=bool))

    if 'land_sea' in ds_r.attrs:
        mask = regionmask.defined_regions.natural_earth.land_110.mask(da) == 1
        if ds_r.attrs['land_sea'] == 'sea_masked':
            mask = ~mask
        da = da.where(mask)

    if 'location' in ds_r.attrs:
        lat, lon = ds_r.attrs['location'].split(', ')[:2]
        lat, lon = float(lat), float(lon)
        idx_lat = np.argmin(da['lat'].data - lat)
        idx_lon = np.argmin(da['lon'].data - lon)
        da = da.isel(lat=idx_lat).isel(lon=idx_lon)
        del ds['lat'], ds['lon']

    if not 'location' in ds_r.attrs:
        da = utils.area_weighted_mean(da.to_dataset())[varn]
        del ds['lat'], ds['lon']

    ds[varn] = da
    return ds

def main():
    utils.set_logger(level=LOGLEVEL)
    read_input()

    global varn
    varn = args.filename.split('_')[0]

    ds_results = xr.open_dataset(os.path.join(DATAPATH, args.filename))
    ds_info = xr.open_dataset(ds_results.attrs['modelfile'])

    models = set(ds_info['model-ensemble'].data).intersection(
        ds_results['model-ensemble'].data)

    models = ['CanESM2-r1i1p1']  # DEBUG

    for mod in models:
        for season_id in ds_results['season_id'].data:

            ds_c = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': mod}).sel(
                scenario='piControl')['filename'].data), decode_cf=False).sel(
                    season_id=season_id)

            ds_26 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': mod}).sel(
                scenario='rcp26')['filename'].data)).sel(season_id=season_id)

            ds_45 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': mod}).sel(
                scenario='rcp45')['filename'].data)).sel(season_id=season_id)

            ds_85 = xr.open_dataset(str(ds_info.sel(**{'model-ensemble': mod}).sel(
                scenario='rcp85')['filename'].data)).sel(season_id=season_id)

            ds_r = ds_results.sel(**{'model-ensemble': mod}).sel(season_id=season_id)

            season = ds_26['season'].data.astype(str)

            ds_c = select_region(ds_c, ds_r)
            ds_26 = select_region(ds_26, ds_r)
            ds_45 = select_region(ds_45, ds_r)
            ds_85 = select_region(ds_85, ds_r)

            target = ds_c[varn]

            # remove mean an possible linear trend due to model drift
            A = np.vstack([np.arange(len(target)), np.ones(len(target))]).T
            m, c = np.linalg.lstsq(A, target, rcond=None)[0]
            target -= m * np.arange(len(target)) + c

            arr_26 = ds_26[varn].data
            fit_26 = lowess(
                ds_r.sel(scenario='rcp26')['window_length_minimum'].data,
                arr_26)

            arr_45 = ds_45[varn].data
            fit_45 = lowess(
                ds_r.sel(scenario='rcp45')['window_length_minimum'].data,
                arr_45)

            arr_85 = ds_85[varn].data
            fit_85 = lowess(
                ds_r.sel(scenario='rcp85')['window_length_minimum'].data,
                arr_85)

            title = 'LOWESS fit overview: {} {}'.format(mod, season)
            if not os.path.isdir(os.path.join(
                    PLOTPATH, 'overview', args.filename.replace('.nc', ''))):
                os.mkdir(os.path.join(PLOTPATH, 'overview', args.filename.replace('.nc', '')))

            filename = 'overview/{}/{}_{}.pdf'.format(
                args.filename.replace('.nc', ''), mod, season)
            plot(ds_r, target, [arr_26, arr_45, arr_85], [fit_26, fit_45, fit_85],
                 filename, title)



if __name__ == '__main__':
    main()
