#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-30 15:19:30 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Boxplots of various dimension.

TODO: Think about what makes sense. -> maybe talk to Erich about it.

"""
import os
import logging
import argparse
import numpy as np
import xarray as xr
from scipy import stats

import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt

import utils.utils as utils

logger = logging.getLogger(__name__)
LOGLEVEL = logging.INFO


DATAPATH = '/net/h2o/climphys/lukbrunn/Data/IsolateNaturalVariability'
PLOTPATH = '/net/h2o/climphys/lukbrunn/Plots/IsolateNaturalVariability/boxplots/'


def window_season(ds):

    def plot_box(ax, arr, labels):
        return ax.boxplot(
        arr,
        # plot vertical
        vert = True,
        # whiskers at 5th & 95 percentila
        whis = [5, 95],
        # box widths
        widths = .9,
        # x labels
        labels = labels,
        # patch_artist=True,
        # median
        medianprops = dict(
            linestyle = '-',
            linewidth = .5,
            color = 'black'),
        # median
        showmeans = True,
        meanline = True,
        meanprops = dict(
            linestyle = '-',
            #linewidth = .5,
            #color = 'red'
        ))

    var = ds['window_length_minimum']
    var = var.isel(ensemble=0)
    var -= var.mean('season_id')
    var = var.mean('scenario')

    fig, axes = plt.subplots(1,3)

    bplot = plot_box(axes[0], var.data, ['DJF', 'MAM', 'JJA', 'SON'])


    var = ds['window_length_minimum']
    var = var.isel(ensemble=0)
    var -= var.mean('scenario')
    var = var.mean('season_id')

    plot_box(axes[1], var.data,  ['RCP 2.6', 'RCP 4.5', 'RCP 8.5'])

    var = ds['window_length_minimum']
    var = var.isel(ensemble=0)

    plot_box(axes[2], var.data.ravel(),  ['all'])

    plt.legend(
        [bplot['medians'][0], bplot['means'][0]],
        ['median', 'mean'])

    plt.show()


def read_config():

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        dest='config', nargs='?', default='DEFAULT',
        help='')
    args = parser.parse_args()
    global cc
    cc = utils.read_config(args.config, './config_boxplot_various.ini')
    utils.log_parser(cc)


def plot_windows(dss, varn, ylim):

    var_list = []
    labels = []

    key_label = {
        'GlobalMean': 'Global',
        'EuropeMean': 'Europe',
        'Zurich': 'Zurich'}

    for key, label in key_label.items():
        var = dss[key].mean(('season_id', 'scenario'))[varn].data
        var_list.append(var)
        labels.append(label)

    var_list.append([])
    labels.append('')

    var = dss['EuropeMean'].mean('scenario')[varn]
    var_list.append(var.sel(season_id=0).data)
    var_list.append(var.sel(season_id=3).data)
    var_list.append(var.sel(season_id=6).data)
    var_list.append(var.sel(season_id=9).data)
    labels += ['DJF', 'MAM', 'JJA', 'SON']

    var_list.append([])
    labels.append('')

    var = dss['EuropeMean'].mean('season_id')[varn]
    # var_list.append(var.sel(scenario='piControl').data)
    var_list.append(var.sel(scenario='rcp26').data)
    var_list.append(var.sel(scenario='rcp45').data)
    var_list.append(var.sel(scenario='rcp85').data)
    labels += [#'Historical',
               'RCP 2.6', 'RCP 4.5', 'RCP 8.5']

    fig, ax = plt.subplots(1, figsize=(11.69,8.27))
    bplot = ax.boxplot(
        var_list,
        # plot vertical
        vert = True,
        # whiskers at 5th & 95 percentila
        whis = [5, 95],
        # box widths
        widths = .9,
        # x labels
        labels = labels,
        # patch_artist=True,
        # median
        medianprops = dict(
            linestyle = '-',
            linewidth = .5,
            color = 'black'),
        # median
        showmeans = True,
        meanline = True,
        meanprops = dict(
            linestyle = '-',
            #linewidth = .5,
            #color = 'red'
        ))

    plt.legend(
        [bplot['medians'][0], bplot['means'][0]],
        ['median', 'mean'])

    ax.set_ylim(*ylim)
    ax.set_ylabel(varn)

    ax.set_xlim(0, len(var_list)+1.)
    ax.set_xlabel('Category')

    # ax.set_title('LOWESS filter window size for different models')

    ax.text(2., ylim[0], 'Scenario mean; Season mean', ha='center', va='bottom')
    ax.text(6.5, ylim[0], 'Europe; Scenario mean', ha='center', va='bottom')
    ax.text(11., ylim[0], 'Europe; Season mean', ha='center', va='bottom')

    plt.savefig(os.path.join(PLOTPATH, '{}_Categories.png'.format(varn)), dpi=300)
    plt.clf()
    plt.close()


def main():
    utils.set_logger()
    read_config()

    if cc.overview is not None:
        dss = {}
        for filename in cc.overview:
            ds = xr.open_dataset(os.path.join(cc.basepath_data, filename))
            region = filename.split('_')[-1].replace('.nc', '')
            ds = ds.sel(scenario=['rcp26', 'rcp45', 'rcp85'])
            if cc.ensemble_members == 'one':
                idx = np.unique(ds.isel(scenario=0)['model'].data, return_index=True)[1]
                ds = ds.isel(**{'model-ensemble': idx})

            dss[region] = ds

        plot_windows(dss, 'window_length_minimum', ylim=(25, 200))
        plot_windows(dss, 'variance', ylim=(-.005, .1))
        plot_windows(dss, 'variance_target', ylim=(-.005, .1))

    # window_season(ds)




if __name__ == '__main__':
    main()
