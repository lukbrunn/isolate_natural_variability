#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-04 08:37:59 lukbrunn>

MIT License

Copyright (c) 2018 Lukas Brunner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:
The main function is 'fit_to_data' it takes an
array and finds the best LOWESS filter (UPDATE: or running average) window size
to match a given target variance. Also contains several helper functions
performing statistical tests and enabling multi-processing.

TODO: look into the effect of lowess: it!
"""

import logging
import numpy as np
import xarray as xr
import statsmodels.api as sm
import multiprocessing as mp
from scipy import stats, signal  # , spatial

from utils.utils import run_parallel

logger = logging.getLogger(__name__)


def bartlett_test(arr1, arr2):
    return stats.bartlett(arr1, arr2).pvalue


def permutation_test(arr1, arr2, permutations=1000, return_scores=False):
    """Permutation test for the null hypothesis that arr1 & arr2 are
    samples from populations with equal variances"""
    arr1, arr2 = np.array(arr1), np.array(arr2)
    score = 1 - np.var(arr1)/np.var(arr2)
    permutation_scores = np.empty(permutations)
    for idx in np.arange(permutations):
        vv1, vv2 = np.array_split(np.random.permutation(
            np.concatenate([arr1, arr2])), [len(arr1)])
        permutation_scores[idx] = 1 - np.var(vv1)/np.var(vv2)
    pvalue = np.sum(
        np.abs(permutation_scores) > np.abs(score)) / float(permutations)
    if return_scores:
        return pvalue, score, permutation_scores
    else:
        return pvalue


def running_mean(window, yy, polyorder=0, mode='mirror'):
    """Fits a smoothed polyorder polynomial to the data. Defaults to a running
    mean.

    Parameters:
    - window (int): Odd integer.
    - yy (array): Array to fit.
    - polyorder=0 (int, optional): Polynomial order to fit. Default (0)
      implements a running mean.
    - mode='mirror' (['mirror' | 'interp'], optional): String setting the
      the handling of the edges.
      * mirror: "Repeats the values at the edges in reverse order. The value
        closest to the edge is not included." -> use this for residuals
      * interp: "A degree polyorder polynomial is fit to the last window
        values of the edges, and this polynomial is used to evaluate the last
        window // 2 output values." -> use this for data

    Returns:
    array"""
    if window == 1:
        return yy
    return signal.savgol_filter(
        yy,
        window_length=window,
        polyorder=polyorder,
        mode=mode)


def lowess(window, yy):
    """Fit a LOWESS filter for a given window size"""
    return sm.nonparametric.lowess(
        endog=yy,
        exog=np.arange(len(yy)),
        frac=float(window) / float(len(yy)),
        it=3,
        delta=0.,
        is_sorted=True,
        missing='raise',
        return_sorted=False,)


def difference_variance(res, target, smoothing_windows, weights=None):
    differences = []
    for window in smoothing_windows:
        arr_var = np.var(running_mean(window, res))
        target_var = np.var(running_mean(window, target))
        difference = (arr_var-target_var) / target_var
        differences.append(difference)
    if weights is None:
        weights = np.ones_like(differences)
    return np.average(np.abs(differences), weights=weights)


# def fit_qualit_spectral(res, target, **kwargs):
#     def bandpass(data):
#         # https://plot.ly/python/fft-filters/#bandpass-filter
#         fL = 0.1
#         fH = 0.3
#         b = 0.08
#         N = int(np.ceil((4 / b)))
#         if not N % 2: N += 1  # Make sure that N is odd.
#         n = np.arange(N)

#         # low-pass filter
#         hlpf = np.sinc(2 * fH * (n - (N - 1) / 2.))
#         hlpf *= np.blackman(N)
#         hlpf = hlpf / np.sum(hlpf)

#         # high-pass filter
#         hhpf = np.sinc(2 * fL * (n - (N - 1) / 2.))
#         hhpf *= np.blackman(N)
#         hhpf = hhpf / np.sum(hhpf)
#         hhpf = -hhpf
#         hhpf[int((N - 1) / 2)] += 1

#         h = np.convolve(hlpf, hhpf)
#         return np.convolve(data, h)

#     qualities = []
#     for window in kwargs['smoothing_windows']:
#         dd1 = running_mean(window, arr - fit)
#         dd2 = running_mean(window, target)
#         f1, pxx1 = signal.welch(bandpass(dd1), fs=1, nperseg=len(dd1)*.5)
#         f2, pxx2 = signal.welch(bandpass(dd2), fs=1, nperseg=len(dd2)*.5)
#         pxx1, f1 = signal.resample(pxx1, len(f2), t=f1)
#         qualities.append(spatial.distance.correlation(pxx1, pxx2))

#     # target = running_mean(11, target)
#     # diff = running_mean(11, arr-fit)
#     # diff = arr-fit
#     # f1, pxx1 = signal.welch(bandpass(target), fs=1, nperseg=target.size*.5)
#     # f2, pxx2 = signal.welch(bandpass(diff), fs=1, nperseg=arr.size*.5)
#     # pxx1, f1 = signal.resample(pxx1, len(f2), t=f1)

#     # import matplotlib.pyplot as plt
#     # plt.plot(f1, pxx1, color='red')
#     # plt.plot(f2, pxx2, color='blue')
#     # plt.gca().set_xlim(0, .5)
#     # plt.show()
#     return np.average(qualities)


def get_difference(res, target, method='variance', **kwargs):
    if method == 'variance':
        return difference_variance(res, target, **kwargs)
    elif method == 'spectral':
        raise NotImplementedError('method={} not implemented'.format(method))
        # return fit_qualit_spectral(res, target, **kwargs)
    else:
        raise NotImplementedError('method={} not implemented'.format(method))


def variance_difference(window, arr, target,
                        smoothing_windows=[1, 21], weights=None,
                        method='lowess', cut_edges=False, queue=None):
    """Compares the residual variance for a given window length
    to the target variance. Variances are compared for different
    characteristic time scales as given by smoothing_windows."""

    if method == 'lowess':
        fit = lowess(window, arr)
    elif method == 'running_mean':
        fit = running_mean(window, arr, 0, 'interp')
    elif isinstance(method, int):
        fit = running_mean(window, arr, method, 'interp')
    else:
        errmsg = ' '.join([
            'method must be one of ["lowess" | "running_mean"',
            '| int] and not {}'.format(method)])
        raise ValueError(errmsg)

    if cut_edges:
        fit = fit[int(window/2.):-int(window/2.)]
        arr = arr[int(window/2.):-int(window/2.)]

    difference = get_difference(
        arr-fit, target, 'variance',
        smoothing_windows=smoothing_windows,
        weights=weights)

    # DEBUG: implement a different measure for the quality of the fit
    # difference = get_fit_quality(arr, fit, target, 'spectral',
    #                            smoothing_windows=[1])

    if queue is None:
        return [window, difference]
    else:
        pid = mp.current_process().pid
        queue.put((pid, [window, difference]))


# TODO
def fit_ds(da, da_target, time_name='time', **kwargs):
    """xarray convenience function

    TODO:
    - write tests
    - write docstring"""
    kwargs['full_output'] = False
    da_target = da_target.rename({time_name: '{}2'.format(time_name)})
    return xr.apply_ufunc(
        np.vectorize(fit_to_data, signature='(n),(m)->(n)'),
        da, da_target,
        input_core_dims=[[time_name], ['{}2'.format(time_name)]],
        output_core_dims=[[time_name]],
        kwargs=kwargs)


def fit_to_data(arr, target,
                method='lowess',
                filter_windows=np.arange(11, 231, 2),
                smoothing_windows=[11, 21, 31],
                weights=None,
                cut_edges=False,
                log_tests=False,
                full_output=False,
                cpus=10):
    """Tests values of filter_windows to minimize the difference between the
    variance of the residuals (of a function given by 'method') and the
    variance of 'target'.

    Parameters:
    - arr (array): (M,) array of values to which the LOWESS filter
      will be fitted.
    - target (array): (N,) array of residuals which will be used to calculate
      the target variance.
    - method='lowess' ([str | int], optional): One of ['lowess' |
      'running_mean' | <int>]. If string gives the fitting method. If int gives
      the polynomial order for the savgol_filter. method=0 is equivalent
      to method='running_mean'.
    - filter_windows=np.arange(11, 251, 2) (array, optional): Array containing
      odd integers. Used as window length for 'method' in the minimizing
      process.
    - smoothing_windows=[11, 21, 31] (array, optional): (L,) array containing
      odd integers. Used to smooth the data before evaluating the quality of
      the fit. If several values are given the average of the relative
      qualities for each smoothing parameter will be taken.
    - weights=None (array, optional): [UNTESTED!] (L,) array with same length
      as smoothing_windows. The values of weights are used in the averaging of
      the different smoothing parameters.
    - cut_edges=False (bool, optional): Determines how the window_length/2
      values on the edges of arr are handled. If True they are ignored in the
      evaluation of the fit quality and filled with np.nan in the output.
    - log_tests=False (bool, optional): Print failing statistical tests
      as logger.warning
    - full_output=False (book, optional): Return additional information in a
      xarray.Dataset().
    - cpus=10 (int, optional): CPU cores to use for multiprocessing when
      trying the values of filter_range.

    Returns:
    fit[, ds]

    - fit (M,) is the fit which minimizes the difference of variances
    - ds is a xarray.Dataset() containing additional information"""
    stat_level = .0001

    arr = np.array(arr)
    target = np.array(target)
    if isinstance(filter_windows, int):
        filter_windows = [filter_windows]
    if isinstance(smoothing_windows, int):
        smoothing_windows = [smoothing_windows]
    if isinstance(weights, (int, float)):
        weights = [weights]

    if np.abs(np.mean(target)) > np.std(target):
        logmsg = 'target mean is more than one StdDev away from zero'
        logger.warning(logmsg)
    if stats.normaltest(target).pvalue < stat_level:
        logmsg = 'target is not normally distributed ({:.2%} level)'.format(
            stat_level)
        logger.warning(logmsg)
        # # ---
        # import matplotlib.pyplot as plt
        # plt.plot(target)
        # plt.show()
        # plt.hist(target)
        # plt.show()
        # print(stats.normaltest(target).pvalue)
        # # ---

    logger.debug('Estimate best window size by minimizing variance difference')
    results = run_parallel(
        variance_difference,
        filter_windows,
        args=(arr, target),
        nr=cpus,
        kwargs=dict(
            smoothing_windows=smoothing_windows,
            method=method,
            cut_edges=cut_edges,
            weights=weights))

    # DEBUG: plot qualities for different methods
    # import matplotlib.pyplot as plt
    # var=[rr[1] for rr in results]
    # ww = [rr[0] for rr in results]
    # q1, q2 = np.swapaxes(var, 0, 1)
    # # plt.plot(ww, q1, color='blue')
    # plt.plot(ww, q2, color='red', ls='none', marker='o')
    # plt.show()
    # import ipdb; ipdb.set_trace()

    # window lengths/qualities can be unsorted if multiprocessing is enabled!
    filter_windows_unsorted, differences = np.swapaxes(results, 0, 1)
    filter_window_min = int(filter_windows_unsorted[np.argmin(differences)])

    if method == 'lowess':
        fit = lowess(filter_window_min, arr)
    elif method == 'running_mean':
        fit = running_mean(filter_window_min, arr, 0, 'interp')
    elif isinstance(method, int):
        fit = running_mean(filter_window_min, arr, method, 'interp')

    if cut_edges:  # remove edges just for calculations
        fit = fit[int(filter_window_min/2.):-int(filter_window_min/2.)]
        arr = arr[int(filter_window_min/2.):-int(filter_window_min/2.)]

    ds = xr.Dataset(
        coords={'smoothing_window': ('smoothing_window', smoothing_windows, {
            'description': ' '.join([
                'Smoothing applied to the residuals and the target before',
                'calculating the variances'])}),
                'filter_window': ('filter_window', filter_windows, {
                    'description': 'Window sizes tested to find the best fit'})},
        data_vars={
            'difference': ('window_length', differences[
                np.argsort(filter_windows_unsorted)],
                        {'description': ' '.join([
                            'Relative difference of the variances: mean of',
                            'relative deviation over smoothing_windows'])}),
            'window_length_minimum': ((), filter_window_min,
                                      {'description': ''.join([
                                          'int(window_length[',
                                          'np.argmin(difference)])'])}),
            # DELETE window_length_minimum? (can be calculated easily)
            'variance': ('smoothing_window',
                         [np.var(running_mean(window, arr-fit))
                          for window in smoothing_windows]),
            'variance_target': ('smoothing_window',
                                [np.var(running_mean(window, target))
                                 for window in smoothing_windows]),
            'bartlett': ('smoothing_window',
                         [bartlett_test(running_mean(window, arr-fit),
                                        running_mean(window, target))
                          for window in smoothing_windows],
                         {'description': 'Bartlett test p-value'}),
            'permutation': ('smoothing_window',
                            [permutation_test(running_mean(window, arr-fit),
                                              running_mean(window, target))
                             for window in smoothing_windows],
                            {'description': 'Permutation test p-value'})},
        attrs={
            'method': method})

    if log_tests:
        if np.abs(np.mean(arr-fit)) > np.std(arr):
            logmsg = ' '.join([
                'Array residual mean is more than one standard deviation',
                'away from zero'])
            logger.info(logmsg)
        if stats.normaltest(arr-fit).pvalue < stat_level:
            logmsg = ' '.join([
                'Array residuals are not normally distributed',
                '({:.0%} level)'.format(stat_level)])
            logger.info(logmsg)
        for window in smoothing_windows:
            ds_sel = ds.sel(smoothing_window=window)
            if np.any([ds_sel['bartlett'] < stat_level,
                       ds_sel['permutation'] < stat_level]):
                logmsg = ' '.join([
                    'Residual variance with window={} is statistically',
                    'significantly different from target variance',
                    '({:.0%} level)'.format(window, stat_level)])
                logger.info(logmsg)

    if cut_edges:  # now fill edges with np.nan
        nanarr = np.empty(int(filter_window_min/2.)) * np.nan
        fit = np.concatenate([nanarr, fit, nanarr])

    if full_output:
        return fit, ds
    return fit
