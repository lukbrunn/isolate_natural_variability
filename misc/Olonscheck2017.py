#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-23 14:57:12 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Recreating some of the results from Olonscheck and Notz 2017.
"""
import os
import xarray as xr
import numpy as np
import logging
from scipy import signal
import matplotlib.pyplot as plt

from utils.get_filenames import Filenames
from utils.utils import set_logger, add_hist, area_weighted_mean

logger = logging.getLogger(__name__)
LOGLEVEL = logging.WARNING


def piC_StdDev():
    """Recreating the preindustrial control run standard deviation.

    Equation (c.f. Eq. 1):
    s(T) = sqrt(1/(T-1) sum((mean(x) - x)**2))

    Data:
    - Annual mean, global mean from monthly mean data.
    - Regridded to a 1.8947x3.75 degree grid

    Known differences in implementation:
    - Direct annual mean -> look into how that has been calculated
    - 2.5x2.5 degree grid
    """

    fn = Filenames('tas/tas_ann_{model}_piControl_{ensemble}_g025.nc')
    fn.apply_filter()

    results = {}
    for model in fn.get_variable_values('model'):
        for ii, ff in enumerate(fn.get_filenames(subset={'model': model})):
            ds = xr.open_dataset(ff, decode_cf=False)
            ds = area_weighted_mean(ds)
            # ds = ds.mean(('lat', 'lon'))

            data = ds['tas'].data

            # remove possible linear trend due to model drift
            A = np.vstack([np.arange(len(data)), np.ones(len(data))]).T
            m, c = np.linalg.lstsq(A, data, rcond=None)[0]
            data -= m * np.arange(len(data)) + c

            # # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.signal.welch.html
            # f, Pxx = signal.welch(data, fs=1, nperseg=data.size*.5)
            # plt.semilogx(f, Pxx)
            # plt.xlim([1e-3, 5e-1])
            # plt.ylim([0, 1])
            # plt.title('{} ({} years)'.format(model, data.size))
            # plt.show()

            result = 2*np.std(ds['tas'].data, ddof=1)
            results[model] = result

            print('{model}-{ens}: Length={ll} years; 2*StdDev={std:.2f} K'.format(
                model=model, ens=ii,
                ll=ds.dims['year'],
                std=result))
    return results


def hist_StdDev():
    """Recreating the preindustrial control run standard deviation.

    Equation (c.f. Eq. 1):
    s(T) = sqrt(1/(T-1) sum((mean(x) - x)**2))

    Data:
    - Annual mean, global mean from monthly mean data.
    - Regridded to a 1.8947x3.75 degree grid

    Known differences in implementation:
    - Direct annual mean -> look into how that has been calculated
    - 2.5x2.5 degree grid
    """

    fn = Filenames('tas/tas_ann_{model}_historicalNat_{ensemble}_g025.nc')
    fn.apply_filter()

    # get only models with >= n_ens members in each scenario
    N_ENS = 2
    del_models = []
    models = fn.get_variable_values('model')
    for model in models:
        if len(fn.get_filenames(subset={'model': model})) < N_ENS:
            del_models.append(model)
    models = list(set(models).difference(del_models))

    results = {}
    for model in models:
        data = []
        for ff in fn.get_filenames(subset={'model': model}):
            ds = xr.open_dataset(ff, decode_cf=False)
            ds = area_weighted_mean(ds)
            # ds = ds.mean(('lat', 'lon'))

            data.append(ds['tas'].data)

        result = 2*np.sqrt(np.mean(np.var(data, axis=0, ddof=1)))
        results[model] = [result, len(fn.get_filenames(subset={'model': model}))]

        print('{model}: Length={ll} years; 2*StdDev={std:.2f} K'.format(
            model=model,
            ll=ds.dims['year'],
            std=result))
    return results


def main():
    set_logger(level=LOGLEVEL)

    ctrl = piC_StdDev()
    hist = hist_StdDev()

    cmap = plt.cm.get_cmap('viridis', len(ctrl))
    colors = list(cmap(np.arange(len(ctrl))))
    for i_model, model in enumerate(ctrl.keys()):
        if model in hist.keys():
            plt.scatter(ctrl[model], hist[model][0], c=colors[i_model],
                        label='{} ({})'.format(model, hist[model][1]))
        else:
            plt.scatter(ctrl[model], .15, c=colors[i_model], label=model)

    plt.legend(bbox_to_anchor=(1.04, 1), loc='upper left', ncol=2, fontsize='xx-small')
    plt.subplots_adjust(right=0.6)
    plt.plot([.15, .4], [.15, .4], ls='--', color='k')
    plt.xlabel('2$\sigma$ piControl')
    plt.ylabel('2$\sigma$ historicalNat')
    plt.show()

if __name__ == '__main__':
    main()
