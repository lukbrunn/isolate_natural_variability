#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-07-30 13:45:57 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import sys
import unittest
import warnings
import logging
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt

sys.path.append('..')
import utils.utils as utils
from estimate_variance import fit_to_data

logging.basicConfig(level=logging.ERROR)


class TestEstimateVariance(unittest.TestCase):

    def setUp(self):
        path = '/net/atmos/data/cmip5-ng/tas'
        ds_hist = xr.open_dataset(
            os.path.join(path, 'tas_sea_CanESM2_piControl_r1i1p1_g025.nc'),
            decode_cf=False)
        ds_forc = xr.open_dataset(
            os.path.join(path, 'tas_sea_CanESM2_rcp26_r1i1p1_g025.nc'))
        ds_hist, ds_forc = utils.area_weighted_mean(ds_hist), utils.area_weighted_mean(ds_forc)
        ds_hist, ds_forc = ds_hist.sel(season_id=0), ds_forc.sel(season_id=0)
        self.ds_hist, self.ds_forc = ds_hist, ds_forc

    def test_input(self):
        pass

    def test_simple(self):
        data_hist = self.ds_hist['tas'].data
        data_forc = self.ds_forc['tas'].data

        # remove mean an possible linear trend due to model drift
        A = np.vstack([np.arange(len(data_hist)), np.ones(len(data_hist))]).T
        m, c = np.linalg.lstsq(A, data_hist, rcond=None)[0]
        data_hist -= m * np.arange(len(data_hist)) + c

        # target has zero variance -> perfect fit (i.e., window=1)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            fit, ds = fit_to_data(data_forc, np.random.randint(-1, 2, len(data_forc))*.1,
                                  filter_windows=[1, 5, 33, 133],
                                  smoothing_windows=1,
                                  full_output=True)
        np.testing.assert_array_equal(data_forc, fit)
        self.assertEqual(ds['window_length_minimum'].data, 1)

        # window=1 not possible -> get lowest possible window
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            fit, ds = fit_to_data(data_forc, np.random.randint(-1, 2, len(data_forc))*.1,
                                  filter_windows=[5, 7, 33, 133],
                                  smoothing_windows=1,
                                  full_output=True)
        self.assertEqual(ds['window_length_minimum'].data, 5)

        # target has huge variance -> get largest possible window
        fit, ds = fit_to_data(data_forc, np.random.randint(-1, 2, len(data_forc))*1.e3,
                              filter_windows=[1, 3, 33, 133],
                              smoothing_windows=1,
                              full_output=True)
        self.assertEqual(ds['window_length_minimum'].data, 133)

        fit, ds = fit_to_data(data_forc, np.random.randint(-1, 2, len(data_forc))*1.e3,
                              filter_windows=[1, 3, 33, 133],
                              full_output=True)
        self.assertEqual(ds['window_length_minimum'].data, 133)

        # simple test for multiprocessing
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            fit_s, ds_s = fit_to_data(data_forc, data_hist, cpus=1, full_output=True)
            fit_m, ds_m = fit_to_data(data_forc, data_hist, full_output=True)
        np.testing.assert_array_equal(fit_s, fit_m)
        np.testing.assert_array_equal(ds_m['difference'].data, ds_s['difference'].data)

    def test_manual(self):
        data_hist = self.ds_hist['tas'].data
        data_forc = self.ds_forc['tas'].data

        # remove mean an possible linear trend due to model drift
        A = np.vstack([np.arange(len(data_hist)), np.ones(len(data_hist))]).T
        m, c = np.linalg.lstsq(A, data_hist, rcond=None)[0]
        data_hist -= m * np.arange(len(data_hist)) + c

        # target has zero variance -> perfect fit (i.e., window=1)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            fit, ds = fit_to_data(data_forc, data_hist,
                                  full_output=True)
        plt.plot(
            self.ds_forc['time'].dt.year.data, data_forc,
            self.ds_forc['time'].dt.year.data, fit)
        plt.show()
        plt.plot(ds['filter_window'], ds['difference'])

        plt.plot([ds['window_length_minimum']], ds['difference'].min(), marker='x')
        plt.gca().set_xlim(ds['window_length_minimum'].data-10, ds['window_length_minimum'].data+10)
        plt.gca().set_ylim(0, 1)
        plt.show()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEstimateVariance)
    unittest.TextTestRunner(verbosity=2).run(suite)
